<?php

/*
Plugin Name:  Flickr Set Slideshow
Description:  Adds automated embed code to convert a slick set to a camerajs slideshow
Author:       Will Kruse<wskruse@gmail.com>
*/

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

define('fss_flickr_api_key', '2d0b9cb6a8e51d0cf2a52c218e82e322');
define('fss_plugin_version', '1.0.0');

wp_embed_register_handler('flickr-set-slideshow', '#http[s]?://www\.flickr\.com/photos/(.+)/sets/(\d+)[/]?#', 'fss_embed_flickr_set');
function fss_embed_flickr_set($matches, $attr, $url, $rawattr) {
	$request_url = sprintf("https://api.flickr.com/services/rest/?method=flickr.photosets.getPhotos&api_key=%s&photoset_id=%s&format=json&nojsoncallback=1", fss_flickr_api_key, $matches[2]);
	$request = new WP_Http;
	$response = $request->request($request_url);
	$json = json_decode($response['body']);
	$photos = $json->photoset->photo;
	$embed = sprintf('<div id="camera_slideshow_%s" class="camera_wrap flickr-set-slideshow">', $matches[2]);
	foreach($photos as $photo) {
		$embed .= sprintf('<div data-src="http://farm%s.staticflickr.com/%s/%s_%s_z.jpg"></div>', $photo->farm, $photo->server, $photo->id, $photo->secret);
	}
	$embed .= "</div>";
	return apply_filters( 'embed_flickr-set-slideshow', $embed, $matches, $attr, $url, $rawattr );
}


function fss_enqueue_scripts() {
  //register camera
	wp_register_script('fss_jqeasing', plugins_url('/js/jquery.easing.1.3.js', __FILE__), array('jquery'), fss_plugin_version, true);
	wp_enqueue_script('fss_jqeasing');
	wp_register_script('fss_jqmcustom', plugins_url('/js/jquery.mobile.customized.min.js', __FILE__), array('jquery'), fss_plugin_version, true);
	wp_enqueue_script('fss_jqmcustom');
  wp_register_script('fss_camerajs', plugins_url('/js/camera.js', __FILE__), array('jquery'), fss_plugin_version, true);
  wp_enqueue_script('fss_camerajs');
  //register the script to activate the camera slideshow
  wp_register_script('fss_customjs', plugins_url('/js/script.js', __FILE__), array('fss_camerajs'), fss_plugin_version, true);
  wp_enqueue_script('fss_customjs');
  //register and enqueue the camera css
  wp_register_style('fss_cameracss', plugins_url('/css/camera.css', __FILE__), array(), fss_plugin_version, 'all');
  wp_enqueue_style('fss_cameracss');
}
add_action('wp_enqueue_scripts', 'fss_enqueue_scripts');
